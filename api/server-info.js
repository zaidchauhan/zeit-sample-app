//
const si = require('systeminformation');

//
const serverInfo = (req, res) => {
  Promise.all([
    si.system(),
    si.osInfo(),
    si.uuid(),
    si.cpu(),
    si.diskLayout()
  ])
  .then(responses => {        
      res.json({
          //system
          manufacturer: responses[0].manufacturer,
          //os
          platform: responses[1].platform,
          distro: responses[1].distro,
          //uuid
          uuid: responses[2].os,
          //cpu
          cpu: responses[3].brand,
          //disks
          disks: responses[4].map(disk => {
              return (({type, name, size}) => ({type, name, size}))(disk);
          })
      });
  })
  .catch(error => {
    console.log('error: ', error);
    res.status(500).end(error.stack);
  });
};

module.exports = serverInfo;